import { Component, OnDestroy, OnInit } from '@angular/core';
import { NbIconLibraries } from '@nebular/theme';
import { Location } from '@angular/common';
import { Module, Node, QuestionNode, StartNode, AnswerNode, EndNode } from '@suitless/module-editor-lib';
import { ModuleEditorService } from '@suitless/module-editor-v2';
import { Subscription } from 'rxjs';
import { environment } from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  public exitLocation: Location;
  public environment: any = environment;
  subscriptions: Subscription[] = [];

  constructor(
    private readonly moduleEditorService: ModuleEditorService,
    private iconLibraries: NbIconLibraries,
    public location: Location
  ) {
    this.iconLibraries.registerFontPack('solid', { packClass: 'fas', iconClassPrefix: 'fa' });
    this.iconLibraries.registerFontPack('regular', { packClass: 'far', iconClassPrefix: 'fa' });
    this.iconLibraries.registerFontPack('light', { packClass: 'far', iconClassPrefix: 'fa' });
    this.iconLibraries.setDefaultPack('solid');

    this.exitLocation = location;
  }

  ngOnInit() {
    const module = new Module('1', 'testModule', '', '', '', '', '', 1);
    const startNode = module.addNode(StartNode, {
      text: 'Start.',
      description: 'This is an example description.',
      flows: [],
    });

    const questionNode = startNode.addNode(
      Node.Create(module, QuestionNode, {
        text: 'Is this a question?',
        multi: false,
        useUniqueRoutes: false,
        explanations: [],
        examples: [],
        answers: [],
        image: '',
      })
    );

    const yesAnswer = questionNode.addNode(
      Node.Create(module, AnswerNode, {
        text: 'Yes.',
        image: '',
      })
    );

    const noAnswer = questionNode.addNode(
      Node.Create(module, AnswerNode, {
        text: 'No.',
        image: '',
      })
    );

    yesAnswer.addNode(
      Node.Create(module, EndNode, {
        text: 'You are right.',
        image: '',
      })
    );

    noAnswer.addNode(
      Node.Create(module, EndNode, {
        text: 'You are wrong.',
        image: '',
      })
    );

    this.moduleEditorService.module = module;
    this.moduleEditorService.selectedNode = startNode;
  }

  ngOnDestroy() {
    for (const sub of this.subscriptions) {
      sub.unsubscribe();
    }
  }
}
