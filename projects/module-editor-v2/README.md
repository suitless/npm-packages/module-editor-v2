# Module Editor V2

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.2.14.

## Code scaffolding

Run `ng generate component component-name --project module-editor-v2` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module --project module-editor-v2`.

> Note: Don't forget to add `--project module-editor-v2` or else it will be added to the default project in your `angular.json` file.

## Build

Run `ng build module-editor-v2` to build the project. The build artifacts will be stored in the `dist/` directory.

## Publishing

After building your library with `ng build module-editor-v2`, go to the dist folder `cd dist/module-editor-v2` and run `npm publish`.

## Running unit tests

Run `ng test module-editor-v2` to execute the unit tests via [Karma](https://karma-runner.github.io).
