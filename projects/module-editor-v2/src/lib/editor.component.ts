import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';
import { AnswerNode, Module, Node, QuestionNode, StartNode, EndNode } from '@suitless/module-editor-lib';
import { ModuleEditorService } from './module-editor.service';
import { IModule } from '@suitless/module-domain';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss'],
})
export class EditorComponent implements OnInit {
  @Input() exitLocation: Location;
  @Input() testPath: string;

  constructor(private readonly moduleEditorService: ModuleEditorService) {}

  ngOnInit(): void {
    const module = new Module('1', 'new module', '', '', '', '', '', 0);
    const startNode = module.addNode(StartNode, {
      text: 'Start.',
      description: 'This is an example description.',
      flows: [],
    });

    const questionNode = startNode.addNode(
      Node.Create(module, QuestionNode, {
        text: 'Is this a question?',
        multi: false,
        useUniqueRoutes: false,
        explanations: [],
        examples: [],
        answers: [],
        image: '',
      })
    );

    const yesAnswer = questionNode.addNode(
      Node.Create(module, AnswerNode, {
        text: 'Yes.',
        image: '',
      })
    );

    const noAnswer = questionNode.addNode(
      Node.Create(module, AnswerNode, {
        text: 'No.',
        image: '',
      })
    );

    yesAnswer.addNode(
      Node.Create(module, EndNode, {
        text: 'You are right.',
        image: '',
      })
    );

    noAnswer.addNode(
      Node.Create(module, EndNode, {
        text: 'You are wrong.',
        image: '',
      })
    );

    this.moduleEditorService.selectedNode = startNode;

    this.moduleEditorService.module = module;

    // Push the module to localstorage when a change happened
    this.moduleEditorService.onModuleChange().subscribe((changedModule) => {
      const changedModuleJSON = JSON.stringify(Module.Export(changedModule));
      localStorage.setItem(changedModule.id, changedModuleJSON);
    });
  }

  saveModule(module: IModule) {
    console.log(module);
  }
}
