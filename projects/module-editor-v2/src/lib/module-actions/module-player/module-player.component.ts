import { Component } from '@angular/core';

@Component({
  selector: 'app-module-player',
  templateUrl: './module-player.component.html',
  styleUrls: ['./module-player.component.scss']
})
export class ModulePlayerComponent {
  module: string;
  testPath: string;
  constructor() {}
}
