import { Component, ElementRef, OnDestroy, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { Location } from '@angular/common';
import { Module, Node, EndNode } from '@suitless/module-editor-lib';
import { IModule } from '@suitless/module-domain';
import { ModuleService, OwnerService, ToasterService } from '@suitless/backend-services';
import { NbDialogService } from '@nebular/theme';

import { saveAs } from 'file-saver';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { encode } from 'url-safe-base64';
import { deflate } from 'pako';

import { ModuleEditorService } from '../module-editor.service';
import { SaveModuleDialogComponent } from './save-module-dialog/save-module-dialog.component';
import { ConfirmExitDialogComponent } from './confirm-exit-dialog/confirm-exit-dialog.component';
import { ModulePlayerComponent } from './module-player/module-player.component';

@Component({
  selector: 'app-module-actions',
  templateUrl: './module-actions.component.html',
  styleUrls: ['./module-actions.component.scss'],
})
export class ModuleActionsComponent implements OnInit, OnDestroy {
  @ViewChild('file') private fileElement: ElementRef<HTMLInputElement>;
  @Input() exitLocation: Location;
  @Input() testPath: string;
  @Input() environment: any;
  @Output() newId = new EventEmitter<string>();

  private readonly destroy$ = new Subject<void>();
  module: Module;

  constructor(
    private readonly moduleEditorService: ModuleEditorService,
    private readonly moduleService: ModuleService,
    private readonly dialogService: NbDialogService,
    private readonly toasterService: ToasterService,
    private readonly ownerService: OwnerService
  ) {}

  ngOnInit(): void {
    this.moduleEditorService
      .onModuleChange()
      .pipe(takeUntil(this.destroy$))
      .subscribe((module) => {
        this.module = module;
      });
  }

  public import() {
    this.fileElement.nativeElement.click();
  }

  public export() {
    const moduleJSON = JSON.stringify(Module.Export(this.module));
    const blob = new Blob([moduleJSON], { type: 'application/json' });
    saveAs(blob, `${this.module.name}.suitless`);
  }

  public save() {
    this.dialogService
      .open(SaveModuleDialogComponent, { context: { module: this.module } })
      .onClose.subscribe((data: Module) => {
        if (!data) {
          return;
        }

        const module = Module.Export(this.module);
        module.owner = this.ownerService.getOwner();

        if (module.id.length > 1) {
          this.moduleService
            .getById(this.environment, module.id, true) // check if module id exists in the database
            .toPromise()
            .then(() => this.updateModule(module))
            .catch(() => this.createModule(module));
        } else {
          this.createModule(module);
        }
      });
  }

  private updateModule(module: IModule) {
    this.moduleService
      .update(this.environment, module)
      .toPromise()
      .then(async (newModule) => {
        this.toasterService.showToast('Nice!', 'saved successfully', 'success');
        this.moduleEditorService.module = await Module.Import(newModule);
      });
  }

  private createModule(module: IModule) {
    this.moduleService
      .create(this.environment, module)
      .toPromise()
      .then((newModule) => {
        this.newId.emit(newModule.id);
        this.toasterService.showToast('Nice!', 'saved successfully', 'success');
      });
  }

  public navigateToDashboard() {
    this.dialogService.open(ConfirmExitDialogComponent).onClose.subscribe((data: boolean) => {
      if (data) {
        this.exitLocation.back();
      }
    });
  }

  public test() {
    const moduleBase64 = encode(btoa(deflate(JSON.stringify(Module.Export(this.module)), { to: 'string' })));
    this.dialogService.open(ModulePlayerComponent, {
      autoFocus: true,
      context: { module: moduleBase64, testPath: this.testPath },
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  async handleModuleImport(): Promise<void> {
    const file = this.fileElement.nativeElement.files[0];
    const fileReader = new FileReader();
    fileReader.onload = async (e) => {
      const text = e.target.result.toString();
      this.moduleEditorService.module = await Module.Import(JSON.parse(text));
      this.moduleEditorService.selectedNode = this.moduleEditorService.module.nodes.first;
      this.fileElement.nativeElement.value = '';
    };
    fileReader.onerror = () => {
      throw new Error('Unable to open file!');
    };
    fileReader.readAsText(file);
  }

  get selectedNode(): Node {
    return this.moduleEditorService.selectedNode;
  }

  get isEndNode(): boolean {
    return this.moduleEditorService.selectedNode instanceof EndNode;
  }
}
