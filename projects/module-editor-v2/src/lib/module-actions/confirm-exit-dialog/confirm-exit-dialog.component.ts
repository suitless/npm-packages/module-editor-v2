import { Component, OnInit } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'lib-confirm-exit-dialog',
  templateUrl: './confirm-exit-dialog.component.html',
  styleUrls: ['./confirm-exit-dialog.component.scss']
})
export class ConfirmExitDialogComponent implements OnInit {
  constructor(protected ref: NbDialogRef<ConfirmExitDialogComponent>, private formBuilder: FormBuilder) {}

  ngOnInit(): void {}

  public cancel() {
    this.ref.close(false);
  }

  public submit() {
    this.ref.close(true);
  }
}
