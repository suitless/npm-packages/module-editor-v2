import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NbDialogRef } from '@nebular/theme';
import { Module } from '@suitless/module-editor-lib';

@Component({
  selector: 'app-save-module-dialog',
  templateUrl: './save-module-dialog.component.html',
  styleUrls: ['./save-module-dialog.component.scss'],
})
export class SaveModuleDialogComponent implements OnInit {
  @Input() module: Module;
  public form: FormGroup;

  constructor(protected ref: NbDialogRef<SaveModuleDialogComponent>, private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      name: [this.module.name || '', [Validators.required]],
      version: [this.module.version + 1 || 1, [Validators.required, Validators.pattern('^[0-9]*$'), Validators.min(1)]],
      description: [this.module.description || '', Validators.required],
      disclaimer: [this.module.disclaimer || '', Validators.required],
      pdfDisclaimer: [this.module.pdfDisclaimer || '', Validators.required],
      exposed: this.module.exposed || false,
    });
  }

  public cancel() {
    this.ref.close();
  }

  public submit() {
    if (this.form.valid) {
      const data = this.form.value;

      this.module.name = data.name;
      this.module.version = data.version;
      this.module.description = data.description;
      this.module.disclaimer = data.disclaimer;
      this.module.pdfDisclaimer = data.pdfDisclaimer;
      this.module.exposed = data.exposed;

      this.ref.close(this.module);
    }
  }

  public addImage(event: any) {
    // TODO: Add image upload support
    // if (event.target.files.length <= 0) {
    //   return;
    // }
    // const file: File = event.target.files[0];
    // // TODO: replace this with cdn server method: getInlineServiceable()
    // // TODO: also upload image to CDN when implementing into suitless-web. But only AFTER dialog is closed.
    // this.module.imageUri =
    //   'https://api.develop.suitless.nl/cdn/suitless/' + file.name.split('.', 1)[0].replace(' ', '_');
  }

  // Form entry getters

  get name() {
    return this.form.get('name');
  }

  get version() {
    return this.form.get('version');
  }

  get description() {
    return this.form.get('description');
  }

  get disclaimer() {
    return this.form.get('disclaimer');
  }

  get pdfDisclaimer() {
    return this.form.get('pdfDisclaimer');
  }

  get imageUri() {
    return this.form.get('imageUri');
  }

  get exposed() {
    return this.form.get('exposed');
  }
}
