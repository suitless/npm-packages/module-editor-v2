import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import {
  CapitalizationConstraint,
  EmptyFieldConstraint,
  EmptyFlowsConstraint,
  Module,
  Node,
  QuestionMarkConstraint,
  SingleAnswerConstraint,
  UniqueAnswerConstraint,
  TerminationConstraint,
} from '@suitless/module-editor-lib';
import { IConstraint, IError } from '@suitless/module-domain';

@Injectable({
  providedIn: 'root',
})
export class ModuleEditorService {
  private module$: BehaviorSubject<Module> = new BehaviorSubject<Module>(null);
  private selectedNode$: BehaviorSubject<Node> = new BehaviorSubject<Node>(null);
  private errors$: BehaviorSubject<IError[]> = new BehaviorSubject<IError[]>(null);

  public refresh() {
    this.module$.next(this.module);
    this.checkErrors();
  }

  public onModuleChange(): Observable<Module> {
    return this.module$.asObservable();
  }

  public get module(): Module {
    return this.module$.getValue();
  }

  public set module(module: Module) {
    this.module$.next(module);
    this.checkErrors();
  }

  public onSelectedNodeChange(): Observable<Node> {
    return this.selectedNode$.asObservable();
  }

  public get selectedNode(): Node {
    return this.selectedNode$.getValue();
  }

  public set selectedNode(node: Node) {
    this.selectedNode$.next(node);
  }

  public onErrorsChange(): Observable<IError[]> {
    return this.errors$.asObservable();
  }

  public checkErrors(): void {
    const constraints: IConstraint[] = [
      new CapitalizationConstraint(),
      new QuestionMarkConstraint(),
      new TerminationConstraint(),
      new UniqueAnswerConstraint(),
      new EmptyFieldConstraint(),
      new EmptyFlowsConstraint(),
      new SingleAnswerConstraint(),
    ];
    let errors: IError[] = [];
    const module = Module.Export(this.module$.value);

    for (const constraint of constraints) {
      if (constraint.global) {
        errors = errors.concat(constraint.run(module));
      } else {
        for (const node of module.nodes) {
          errors = errors.concat(constraint.run(node));
        }
      }
    }

    return this.errors$.next(errors);
  }
}
