import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModuleErrorComponent } from './module-error.component';

describe('ModuleErrorComponent', () => {
  let component: ModuleErrorComponent;
  let fixture: ComponentFixture<ModuleErrorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModuleErrorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModuleErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
