import { Component, OnInit, OnDestroy } from '@angular/core';
import { IError } from '@suitless/module-domain';
import { Subject } from 'rxjs';
import { ModuleEditorService } from '../module-editor.service';
import { Module } from '@suitless/module-editor-lib';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-module-error',
  templateUrl: './module-error.component.html',
  styleUrls: ['./module-error.component.scss'],
})
export class ModuleErrorComponent implements OnInit, OnDestroy {
  private readonly destroy$ = new Subject<void>();
  shown = true;
  module: Module;
  errors: IError[];

  constructor(private readonly moduleEditorService: ModuleEditorService) {}

  public selectNode(selectId: number) {
    if (this.module) {
      const node = this.module.nodes.get(selectId);
      if (node) {
        this.moduleEditorService.selectedNode = node;
      }
    }
  }

  ngOnInit(): void {
    this.moduleEditorService
      .onModuleChange()
      .pipe(takeUntil(this.destroy$))
      .subscribe((module) => {
        this.module = module;
      }),
      this.moduleEditorService
        .onErrorsChange()
        .pipe(takeUntil(this.destroy$))
        .subscribe((errors) => {
          if (errors) {
            errors.sort((a, b) => {
              return a.severeness.valueOf() - b.severeness.valueOf();
            });
          }
          this.errors = errors;
        });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
  toggleShown() {
    this.shown = !this.shown;
  }
}
