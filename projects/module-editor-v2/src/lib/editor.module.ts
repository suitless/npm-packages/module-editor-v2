import { NgModule } from '@angular/core';

import { EditorComponent } from './editor.component';
import { ModuleGraphComponent } from './module-graph/module-graph.component';
import {
  NbAccordionModule,
  NbActionsModule,
  NbButtonModule,
  NbBadgeModule,
  NbCardModule,
  NbContextMenuModule,
  NbDialogModule,
  NbIconModule,
  NbInputModule,
  NbLayoutModule,
  NbListModule,
  NbMenuModule,
  NbSelectModule,
  NbSidebarModule,
  NbToggleModule,
  NbUserModule,
  NbCheckboxModule,
  NbProgressBarModule,
} from '@nebular/theme';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ModuleActionsComponent } from './module-actions/module-actions.component';
import { ModuleInputComponent } from './module-input/module-input.component';
import { ModuleHeaderActionsComponent } from './module-header-actions/module-header-actions.component';
import { SaveModuleDialogComponent } from './module-actions/save-module-dialog/save-module-dialog.component';
import { ModulePlayerComponent } from './module-actions/module-player/module-player.component';
import { SafePipe } from './safe.pipe';
import { ModuleErrorComponent } from './module-error/module-error.component';
import { CommonModule } from '@angular/common';
import { ConfirmExitDialogComponent } from './module-actions/confirm-exit-dialog/confirm-exit-dialog.component';
import { PopoutTextareaComponent } from './module-input/popout-textarea/popout-textarea.component';
import { ImageUploadComponent } from './module-input/image-upload/image-upload.component';

@NgModule({
  declarations: [
    EditorComponent,
    ModuleGraphComponent,
    ModuleActionsComponent,
    ModuleInputComponent,
    ModuleHeaderActionsComponent,
    SaveModuleDialogComponent,
    ModulePlayerComponent,
    SafePipe,
    ModuleErrorComponent,
    ConfirmExitDialogComponent,
    PopoutTextareaComponent,
    ImageUploadComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    NbDialogModule.forChild(),
    NbLayoutModule,
    NbCardModule,
    NbInputModule,
    NbAccordionModule,
    NbButtonModule,
    NbBadgeModule,
    ReactiveFormsModule,
    NbSelectModule,
    NbToggleModule,
    NbListModule,
    NbActionsModule,
    NbIconModule,
    NbUserModule,
    NbContextMenuModule,
    NbCheckboxModule,
    NbProgressBarModule,
  ],
  exports: [
    EditorComponent,
    ModuleGraphComponent,
    ModuleActionsComponent,
    ModuleInputComponent,
    ModuleHeaderActionsComponent,
    SaveModuleDialogComponent,
    ModulePlayerComponent,
    ModuleErrorComponent,
  ],
  providers: [],
  entryComponents: [SaveModuleDialogComponent, ModulePlayerComponent, PopoutTextareaComponent],
})
export class EditorModule {}
