import { AfterViewInit, Component, ElementRef, HostListener, OnDestroy } from '@angular/core';
import * as G6 from '@antv/g6';
import { Graph } from '@antv/g6';
import { ModuleEditorService } from '../module-editor.service';
import { Subject, Subscription } from 'rxjs';
import { GraphData } from '@antv/g6/lib/types';
import {
  AnswerNode,
  ImplicationNode,
  Module,
  Node,
  NotificationNode,
  QuestionNode,
  StartNode,
  EndNode,
} from '@suitless/module-editor-lib';
import { G6GraphEvent } from '@antv/g6/lib/interface/behavior';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-module-graph',
  templateUrl: './module-graph.component.html',
  styleUrls: ['./module-graph.component.scss'],
})
export class ModuleGraphComponent implements AfterViewInit, OnDestroy {
  constructor(private readonly el: ElementRef, private readonly moduleEditorService: ModuleEditorService) {}
  private graph: Graph;
  private readonly destroy$ = new Subject<void>();

  /**
   * Render the module in a graph
   * @param module The module to render
   * @param graph The graph to render in
   */
  private static renderModule(module: Module, graph: Graph): void {
    const graphData: GraphData = {
      nodes: [],
      edges: [],
      combos: [],
    };

    const nodes = module.nodes.toValueArray();

    for (const node of nodes) {
      switch (node.constructor) {
        case QuestionNode:
          const comboId = `${node.id}-combo`;
          graphData.combos.push({
            id: comboId,
            type: 'rect',
          });
          graphData.nodes.push({
            id: node.id.toString(),
            label: this.TextToLabel(`(${node.id}) ${node.text}`),
            size: 50,
            type: 'circle',
            style: {
              radius: 0,
              fill: 'snow',
            },
            comboId,
          });
          break;
        case AnswerNode:
          graphData.nodes.push({
            id: node.id.toString(),
            label: this.TextToLabel(`(${node.id}) ${node.text}`),
            type: 'rect',
            size: [100, 30],
            style: {
              radius: 0,
              fill: 'snow',
            },
            comboId: `${node.parents.first.id}-combo`,
          });
          break;
        case ImplicationNode:
          graphData.nodes.push({
            id: node.id.toString(),
            label: this.TextToLabel(`(${node.id}) ${node.text}`),
            type: 'rect',
            size: [100, 30],
            style: {
              radius: 7,
              fill: 'mistyrose',
            },
          });
          break;
        case NotificationNode:
          graphData.nodes.push({
            id: node.id.toString(),
            label: this.TextToLabel(`(${node.id}) ${node.text}`),
            type: 'rect',
            size: [100, 30],
            style: {
              radius: 7,
              fill: 'linen',
            },
          });
          break;
        case StartNode:
        case EndNode:
          graphData.nodes.push({
            id: node.id.toString(),
            label: this.TextToLabel(`(${node.id}) ${node.text}`),
            type: 'diamond',
            size: 50,
            style: {
              radius: 7,
              fill: 'lavender',
            },
          });
          break;
      }

      for (const flow of node.flows.values()) {
        graphData.edges.push({
          source: node.id.toString(),
          target: flow.id.toString(),
        });
      }
    }

    graph.changeData(graphData);
  }

  /**
   * Change focus and styles when the selected node changes
   * @param node The newly selected node
   * @param graph The graph to change
   */
  private static handleSelectedNodeChange(node: Node, graph: Graph): void {
    const graphNode = graph.findById(node.id.toString());

    // Deselect other nodes
    graph.findAllByState('node', 'active').forEach((foundItem) => {
      if (foundItem._cfg.id !== node.id.toString()) {
        graph.setItemState(foundItem, 'active', false);
      }
    });

    setTimeout(() => {
      //  Set the active state to true
      graph.setItemState(graphNode, 'active', true);
    }, 1);

    // TODO: Enable this when it is fixed. There is a bug where this will not center the item in the screen with bigger modules.
    // graph.focusItem(graphNode, true, {
    //   easing: 'easeCubic',
    //   duration: 300,
    // });
  }

  /**
   * Cuts text to the size of a label that fit in the G6 graph.
   * @param input input text
   * @param maxWidth width of the label
   * @param maxHeight height of the label
   * @param fontSize font size
   */
  private static TextToLabel(input: string, maxWidth: number = 140, maxHeight: number = 30, fontSize: number = 12) {
    const ellipsis = '...';
    let currentWidth = 0;
    let output = '';
    let lastCut = 0;
    let length = 0;
    for (const char of input.split('')) {
      currentWidth += G6.Util.getLetterWidth(char, fontSize);
      if (currentWidth > maxWidth) {
        output = output.concat(input.substr(lastCut, length), '-\n');
        currentWidth = 0;
        lastCut += length;
        length = 0;

        if (output.split('\n').length * fontSize >= maxHeight) {
          output = output.substr(0, output.length - 2).concat(ellipsis);
          return output;
        }
      }
      length++;
    }
    output = output.concat(input.substr(lastCut));

    return output;
  }

  ngAfterViewInit(): void {
    const { offsetWidth, offsetHeight } = this.el.nativeElement;

    this.graph = new G6.Graph({
      container: 'module-graph-mount',
      width: offsetWidth,
      height: offsetHeight,
      layout: {
        type: 'dagre',
        rankdir: 'LR',
        controlPoints: true,
        workerEnabled: true,
      },
      plugins: [new G6.Grid()],
      modes: {
        default: ['drag-canvas', 'zoom-canvas'],
      },
      defaultNode: {
        style: {
          cursor: 'pointer',
        },
        stateStyles: {
          active: {
            stroke: '#8c6000',
            fill: '#f2b800',
            fillOpacity: 0.7,
          },
        },
      },
      defaultEdge: {
        style: {
          stroke: '#0b3b65',
          endArrow: {
            path: 'M 0,0 L 8,4 L 8,-4 Z',
            fill: '#0b3b65',
          },
        },
      },
    });
    // Bind graph events
    this.graph.on('node:click', (event) => {
      if (this.moduleEditorService.selectedNode?.id !== Number(event.item._cfg.id)) {
        this.handleNodeClick(event);
      }
    });

    this.moduleEditorService
      .onModuleChange()
      .pipe(takeUntil(this.destroy$))
      .subscribe((module) => {
        ModuleGraphComponent.renderModule(module, this.graph);
      }),
      this.moduleEditorService
        .onSelectedNodeChange()
        .pipe(takeUntil(this.destroy$))
        .subscribe((node) => {
          if (node) {
            ModuleGraphComponent.handleSelectedNodeChange(node, this.graph);
          }
        });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  /**
   * Change the selected node based on a canvas click
   * @param event The event emitted by the graph
   */
  private handleNodeClick(event: G6GraphEvent): void {
    const module = this.moduleEditorService.module;
    this.moduleEditorService.selectedNode = module.nodes.get(Number(event.item._cfg.id));
  }

  /**
   * Updates the graph size based on the available space
   */
  @HostListener('window:resize')
  onResize() {
    const { offsetWidth, offsetHeight } = this.el.nativeElement;
    this.graph.changeSize(offsetWidth, offsetHeight);
  }
}
