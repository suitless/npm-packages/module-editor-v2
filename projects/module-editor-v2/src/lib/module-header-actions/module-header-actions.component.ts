import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ModuleEditorService } from '../module-editor.service';
import { Module } from '@suitless/module-editor-lib';

@Component({
  selector: 'app-module-header-actions',
  templateUrl: './module-header-actions.component.html',
  styleUrls: ['./module-header-actions.component.scss']
})
export class ModuleHeaderActionsComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  module: Module;

  constructor(private readonly moduleEditorService: ModuleEditorService) {}

  ngOnInit(): void {
    this.subscriptions.push(
      this.moduleEditorService.onModuleChange().subscribe(module => {
        this.module = module;
      })
    );
  }

  get name(): string {
    if (this.module) {
      return this.module.name;
    } else {
      return '';
    }
  }

  ngOnDestroy(): void {
    for (const subscription of this.subscriptions) {
      subscription.unsubscribe();
    }
  }
}
