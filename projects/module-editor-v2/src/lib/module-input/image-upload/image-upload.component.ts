import { Component, ElementRef, Input, forwardRef, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { CdnService, OwnerService, ServiceableWrapper, ToasterService } from '@suitless/backend-services';

@Component({
  selector: 'image-upload',
  templateUrl: './image-upload.component.html',
  styleUrls: ['./image-upload.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ImageUploadComponent),
      multi: true,
    },
  ],
})
export class ImageUploadComponent implements ControlValueAccessor, AfterViewInit {
  @ViewChild('file') private fileElement: ElementRef<HTMLInputElement>;
  @ViewChild('image') private imgElement: ElementRef<HTMLImageElement>;
  @ViewChild('preview') private imgPreviewElement: ElementRef<HTMLImageElement>;
  @Input() environment: any;
  private imageId: string;
  private propagateChange = (_: any) => {};

  constructor(
    private readonly cdnService: CdnService,
    private readonly ownerService: OwnerService,
    private readonly toasterService: ToasterService
  ) {}

  public ngAfterViewInit() {
    if (this.imageId) {
      this.imgElement.nativeElement.src = `${this.environment.apiUrl}/cdn/${this.imageId}`;
      this.imgPreviewElement.nativeElement.src = '';
    } else {
      this.imgElement.nativeElement.src = '';
      this.imgPreviewElement.nativeElement.src = '';
    }
  }

  public writeValue(value: string): void {
    this.imageId = value;
    if (this.imgElement) {
      if (this.imageId) {
        this.imgElement.nativeElement.src = `${this.environment.apiUrl}/cdn/${this.imageId}`;
        this.imgPreviewElement.nativeElement.src = '';
      } else {
        this.imgElement.nativeElement.src = '';
        this.imgPreviewElement.nativeElement.src = '';
      }
    }
  }
  public registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  public registerOnTouched(fn: any): void {}

  get isSelected() {
    return this.imageId && this.imageId.length !== 0;
  }

  public open() {
    this.fileElement.nativeElement.click();
  }

  public close() {
    this.imageId = null;
    this.propagateChange(this.imageId);
    this.imgPreviewElement.nativeElement.src = '';
    this.imgElement.nativeElement.src = '';
  }

  public handleImageUpload() {
    const file = this.fileElement.nativeElement.files[0];
    const fileReader = new FileReader();
    fileReader.onload = async (e) => {
      this.imgPreviewElement.nativeElement.src = e.target.result as string;

      this.toasterService.showToast('Uploading image...', '', 'primary');

      this.cdnService
        .uploadUniqueServiceable(
          this.environment,
          {
            name: 'module',
            data: file,
            mimeType: file.type,
          },
          this.ownerService.getOwner()
        )
        .then((wrapper: ServiceableWrapper) => {
          this.toasterService.showToast('Image uploaded!', '', 'success');
          this.imageId = wrapper.id;
          this.imgPreviewElement.nativeElement.src = '';
          this.imgElement.nativeElement.src = `${this.environment.apiUrl}/cdn/${this.imageId}`;
          this.propagateChange(this.imageId);
        })
        .catch((result) => {
          this.imgPreviewElement.nativeElement.src = '';
          if (result.statusText) {
            this.toasterService.error('Unable to upload image', result.statusText);
          } else {
            this.toasterService.error('Unable to upload image', result);
          }
        });
    };
    fileReader.onerror = () => {
      this.toasterService.error('Unable to open image', '');
      throw new Error('Unable to open file!');
    };
    fileReader.readAsDataURL(file);
  }
}
