import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ModuleEditorService } from '../module-editor.service';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import {
  AnswerNode,
  ImplicationNode,
  Node,
  QuestionNode,
  StartNode,
  NotificationNode,
  Module,
  EndNode,
} from '@suitless/module-editor-lib';
import { NbMenuItem, NbMenuService, NbDialogService } from '@nebular/theme';
import { Subscription } from 'rxjs';
import { PopoutTextareaComponent } from './popout-textarea/popout-textarea.component';

@Component({
  selector: 'app-module-input',
  templateUrl: './module-input.component.html',
  styleUrls: ['./module-input.component.scss'],
})
export class ModuleInputComponent implements OnInit, OnDestroy {
  @Input() environment: any;

  constructor(
    private readonly formBuilder: FormBuilder,
    public readonly moduleEditorService: ModuleEditorService,
    private readonly menuService: NbMenuService,
    private readonly dialogService: NbDialogService
  ) {}

  get f() {
    return this.inputForm.controls;
  }

  get isStartNode(): boolean {
    return this.moduleEditorService.selectedNode instanceof StartNode;
  }

  get isQuestionNode(): boolean {
    return this.moduleEditorService.selectedNode instanceof QuestionNode;
  }

  get isAnswerNode(): boolean {
    return this.moduleEditorService.selectedNode instanceof AnswerNode;
  }

  get isNotificationNode(): boolean {
    return this.moduleEditorService.selectedNode instanceof NotificationNode;
  }

  get isImplicationNode(): boolean {
    return this.moduleEditorService.selectedNode instanceof ImplicationNode;
  }

  get isEndNode(): boolean {
    return this.moduleEditorService.selectedNode instanceof EndNode;
  }

  get availableNodes(): Node[] {
    const selectedNode = this.moduleEditorService.selectedNode;
    return this.moduleEditorService.module.nodes.toValueArray().filter((node) => {
      if (selectedNode !== node) {
        if (node instanceof AnswerNode) {
          return !node.parents.has(selectedNode.id);
        } else if (node instanceof EndNode) {
          return false;
        } else {
          return !(node instanceof QuestionNode);
        }
      }

      return false;
    });
  }
  private readonly subscriptions: Subscription[] = [];
  inputForm: FormGroup;
  module: Module;

  addNodeMenuItems: { [key: string]: NbMenuItem[] } = {
    [StartNode.name]: [
      {
        title: 'Question',
        data: QuestionNode.prototype,
      },
      {
        title: 'Implication',
        data: ImplicationNode.prototype,
      },
      {
        title: 'Notification',
        data: NotificationNode.prototype,
      },
      {
        title: 'End',
        data: EndNode.prototype,
      },
    ],
    [QuestionNode.name]: [
      {
        title: 'Answer',
        data: AnswerNode.prototype,
      },
    ],
    [AnswerNode.name]: [
      {
        title: 'Question',
        data: QuestionNode.prototype,
      },
      {
        title: 'Implication',
        data: ImplicationNode.prototype,
      },
      {
        title: 'Notification',
        data: NotificationNode.prototype,
      },
      {
        title: 'End',
        data: EndNode.prototype,
      },
    ],
    [ImplicationNode.name]: [
      {
        title: 'Question',
        data: QuestionNode.prototype,
      },
      {
        title: 'Implication',
        data: ImplicationNode.prototype,
      },
      {
        title: 'Notification',
        data: NotificationNode.prototype,
      },
      {
        title: 'End',
        data: EndNode.prototype,
      },
    ],
    [NotificationNode.name]: [
      {
        title: 'Question',
        data: QuestionNode.prototype,
      },
      {
        title: 'Implication',
        data: ImplicationNode.prototype,
      },
      {
        title: 'Notification',
        data: NotificationNode.prototype,
      },
      {
        title: 'End',
        data: EndNode.prototype,
      },
    ],
  };

  ngOnInit(): void {
    this.subscriptions.push(
      this.menuService.onItemClick().subscribe(({ tag, item }) => {
        if (tag === 'module-actions-add') {
          const selectedNode = this.moduleEditorService.selectedNode;
          if (selectedNode) {
            const node = Node.Create(this.module, item.data.constructor, {
              text: item.data.constructor.name,
              image: '',
            }) as Node;
            selectedNode.addNode(node);
            this.moduleEditorService.refresh();
            setTimeout(() => {
              this.moduleEditorService.selectedNode = node;
            }, 200);
          }
        }
      }),
      this.moduleEditorService.onModuleChange().subscribe((module) => {
        this.module = module;
      }),
      this.moduleEditorService.onSelectedNodeChange().subscribe((selectedNode) => {
        if (selectedNode) {
          this.inputForm = this.formBuilder.group({
            text: [selectedNode.text, [Validators.required]],
            parents: [[...selectedNode.parents.map((parent) => parent.id)]],
          });

          const anySelectedNode = selectedNode as any;

          if (this.isStartNode) {
            this.inputForm.addControl('description', this.formBuilder.control(anySelectedNode.description));
          }

          if (this.isQuestionNode) {
            const examples = [];
            for (const example of anySelectedNode.examples) {
              examples.push(
                this.formBuilder.group({
                  text: [example, [Validators.required]],
                })
              );
            }
            const explanations = [];
            for (const explanation of anySelectedNode.explanations) {
              explanations.push(
                this.formBuilder.group({
                  text: [explanation, [Validators.required]],
                })
              );
            }
            this.inputForm.addControl('examples', this.formBuilder.array(examples));
            this.inputForm.addControl('explanations', this.formBuilder.array(explanations));
            this.inputForm.addControl('multi', this.formBuilder.control(anySelectedNode.multi));
            this.inputForm.addControl('useUniqueRoutes', this.formBuilder.control(anySelectedNode.useUniqueRoutes));
          }

          if (this.isImplicationNode) {
            this.inputForm.addControl(
              'implicationLevel',
              this.formBuilder.control(anySelectedNode.level, Validators.required)
            );
          }

          if (this.isEndNode) {
            this.inputForm.addControl('force', this.formBuilder.control(anySelectedNode.force, Validators.required));
          }

          if (this.isAnswerNode || this.isNotificationNode || this.isQuestionNode) {
            this.inputForm.addControl('image', this.formBuilder.control(anySelectedNode.image));
          }
        }
      })
    );
  }

  ngOnDestroy(): void {
    for (const subscription of this.subscriptions) {
      subscription.unsubscribe();
    }
  }

  handleFormSubmit(): void {
    if (this.inputForm.valid) {
      const selectedNode = this.moduleEditorService.selectedNode as any;
      const {
        text,
        parents,
        description,
        multi,
        implicationLevel,
        useUniqueRoutes,
        force,
        image,
        examples,
        explanations,
      } = this.inputForm.value;
      selectedNode.text = text;

      if (this.isStartNode) {
        selectedNode.description = description;
      }

      if (this.isQuestionNode) {
        selectedNode.examples = [];
        for (const example of examples) {
          selectedNode.examples.push(example.text);
        }
        selectedNode.explanations = [];
        for (const explanation of explanations) {
          selectedNode.explanations.push(explanation.text);
        }
        selectedNode.multi = multi;
        selectedNode.useUniqueRoutes = useUniqueRoutes;
      }

      if (this.isImplicationNode) {
        selectedNode.level = implicationLevel;
      }

      if (this.isEndNode) {
        selectedNode.force = force;
      }

      if (!this.isStartNode) {
        if (selectedNode.parents.toKeyArray().sort().join(',') !== parents.sort().join(',')) {
          // Remove nodes
          const toRemove = selectedNode.parents.toKeyArray().filter((x) => !parents.includes(x));
          for (const nodeID of toRemove) {
            const node = this.moduleEditorService.module.nodes.get(nodeID);
            selectedNode.parents.delete(nodeID);
            node.flows.delete(selectedNode.id);
          }

          // Add nodes
          const toAdd = parents.filter((x) => !selectedNode.parents.toKeyArray().includes(x));
          for (const nodeID of toAdd) {
            const node = this.moduleEditorService.module.nodes.get(nodeID);
            selectedNode.parents.set(node.id, node);
            node.flows.set(selectedNode.id, selectedNode);
          }
        }
      }

      if (this.isAnswerNode || this.isNotificationNode || this.isQuestionNode) {
        selectedNode.image = image;
      }

      this.moduleEditorService.refresh();
      this.moduleEditorService.selectedNode = selectedNode;
    }
  }

  handleNodeRemove() {
    const selectedNode = this.moduleEditorService.selectedNode;

    if (selectedNode instanceof QuestionNode) {
      for (const flow of selectedNode.flows.toValueArray()) {
        flow.remove();
      }
    }
    selectedNode.remove();
    const firstParent = selectedNode.parents.first;
    if (firstParent) {
      this.moduleEditorService.selectedNode = firstParent;
    } else {
      this.moduleEditorService.selectedNode = null;
    }

    this.moduleEditorService.refresh();
  }

  get examples(): FormArray {
    return this.inputForm.get('examples') as FormArray;
  }

  get explanations(): FormArray {
    return this.inputForm.get('explanations') as FormArray;
  }

  addExample() {
    this.examples.push(
      this.formBuilder.group({
        text: ['Example...', [Validators.required]],
      })
    );
  }

  removeExample(index: number) {
    this.examples.removeAt(index);
  }

  addExplanation() {
    this.explanations.push(
      this.formBuilder.group({
        text: ['Explanation...', [Validators.required]],
      })
    );
  }

  removeExplanation(index: number) {
    this.explanations.removeAt(index);
  }

  popoutText() {
    const { text } = this.inputForm.value;

    this.dialogService.open(PopoutTextareaComponent, { context: { text } }).onClose.subscribe((popOutText) => {
      if (popOutText) {
        this.inputForm.controls['text'].setValue(popOutText);
        this.inputForm.markAsDirty();
      }
    });
  }
}
