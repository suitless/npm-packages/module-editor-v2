import { Component, OnInit, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';

@Component({
  selector: 'lib-popout-textarea',
  templateUrl: './popout-textarea.component.html',
  styleUrls: ['./popout-textarea.component.css'],
})
export class PopoutTextareaComponent implements OnInit {
  @Input() text: string;

  constructor(private readonly dialog: NbDialogRef<PopoutTextareaComponent>) {}

  ngOnInit(): void {}

  close() {
    this.dialog.close(this.text);
  }

  discard() {
    this.dialog.close();
  }
}
