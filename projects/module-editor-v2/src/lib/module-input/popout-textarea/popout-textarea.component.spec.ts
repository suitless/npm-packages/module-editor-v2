import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopoutTextareaComponent } from './popout-textarea.component';

describe('PopoutTextareaComponent', () => {
  let component: PopoutTextareaComponent;
  let fixture: ComponentFixture<PopoutTextareaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopoutTextareaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopoutTextareaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
