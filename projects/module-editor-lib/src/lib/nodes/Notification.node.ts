import { ENodeType, INotificationNode, INotificationNodeData } from '@suitless/module-domain';

import { Node } from './Node';
import { Module } from '../Module';

export class NotificationNode extends Node {
  public image: string;

  constructor(module: Module, id: number, input: INotificationNodeData) {
    super(module, { id, name: 'Notification Node', text: input.text });
    this.image = input.image ? input.image : '';
  }

  build(): INotificationNode {
    const { id, text, flows, image } = this;

    return {
      id,
      type: ENodeType.Notification,
      typeData: {
        text,
        image,
        flows: flows.toKeyArray(),
      },
    };
  }
}
