import { ENodeType, IImplicationNode, IImplicationNodeData } from '@suitless/module-domain';

import { Node } from './Node';
import { Module } from '../Module';

export class ImplicationNode extends Node {
  public level: string;

  constructor(module: Module, id: number, input: IImplicationNodeData) {
    super(module, { id, name: 'Implication Node', text: input.text });

    this.level = input.level ? input.level : 'assumptions';
  }

  build(): IImplicationNode {
    const { id, text, level, flows } = this;

    return {
      id,
      type: ENodeType.Implication,
      typeData: {
        text,
        level,
        flows: flows.toKeyArray(),
      },
    };
  }
}
