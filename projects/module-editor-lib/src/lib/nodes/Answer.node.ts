import { Node } from './Node';
import { IAnswer } from '@suitless/module-domain';
import { Module } from '../Module';
import { IAnswerNodeData } from '../interfaces';

export class AnswerNode extends Node {
  public image: string;

  constructor(module: Module, id: number, input: IAnswerNodeData) {
    super(module, { id, name: 'Answer Node', text: input.text });

    this.image = input.image;
  }

  build(): IAnswer {
    const { id, text, flows, image } = this;

    return {
      id,
      text,
      image,
      flows: flows.toKeyArray(),
    };
  }
}
