import { Module } from '../Module';
import { IAnswer, INode } from '@suitless/module-domain';
import { INodeOptions, NodeInput } from '../interfaces';
import { Cache } from '../utils';

export abstract class Node {
  public readonly id: number;
  public readonly name: string;
  public readonly module: Module;
  public text: string;
  public parents: Cache<number, Node>;
  public flows: Cache<number, Node>;

  protected constructor(module: Module, nodeOptions: INodeOptions) {
    this.module = module;
    this.id = nodeOptions.id;
    this.name = nodeOptions.name;
    this.text = nodeOptions.text;

    this.flows = new Cache<number, Node>();
    this.parents = new Cache<number, Node>();
  }

  /**
   * Creates a new node
   * @param module The module that will contain the node
   * @param type The type of node to create
   * @param input The node data
   */
  public static Create<N>(
    module: Module,
    type: new (module: Module, id: number, input: NodeInput) => N,
    input: NodeInput
  ): N {
    // Find and sort the used ID's
    const usedIDs = module.nodes.toKeyArray().sort((a, b) => a - b);

    // Find the lowest unused ID
    let lowestID = -1;
    for (let i = 0; i < usedIDs.length; i++) {
      if (usedIDs[i] !== i) {
        lowestID = i;
        break;
      }
    }

    if (usedIDs.length === 0) {
      lowestID = 0;
    } else {
      if (lowestID === -1) {
        lowestID = usedIDs[usedIDs.length - 1] + 1;
      }
    }

    return new type(module, lowestID, input);
  }

  /**
   * Adds a new node to the flows
   * @param node The node to add to the flows
   */
  public addNode(node: Node): Node {
    this.flows.set(node.id, node);
    node.parents.set(this.id, this);
    this.module.nodes.set(node.id, node);
    return node;
  }

  /**
   * Completely removes the node
   */
  public remove(): void {
    // Remove node from flows
    this.flows.map((flow) => {
      flow.parents.delete(this.id);
    });

    // Remove node from parents
    this.parents.map((parent) => {
      parent.flows.delete(this.id);
    });

    this.module.nodes.delete(this.id);
  }

  /**
   * Builds the node to a universal format
   */
  abstract build(): INode | IAnswer;
}
