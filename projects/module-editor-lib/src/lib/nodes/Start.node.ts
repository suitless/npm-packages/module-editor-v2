import { Node } from './Node';
import { ENodeType, IStartNode, IStartNodeData } from '@suitless/module-domain';
import { Module } from '../Module';

export class StartNode extends Node {
  public description: string;

  constructor(module: Module, id: number, input: IStartNodeData) {
    super(module, { id, name: 'Start Node', text: input.text });

    this.description = input.description ? input.description : '';
  }

  build(): IStartNode {
    const { id, text, description, flows } = this;
    return {
      id,
      type: ENodeType.Start,
      typeData: {
        text,
        description,
        flows: flows.toKeyArray(),
      },
    };
  }
}
