import { ENodeType, IEndNode, IEndNodeData } from '@suitless/module-domain';

import { Node } from './Node';
import { Module } from '../Module';

export class EndNode extends Node {
  public force: boolean;

  constructor(module: Module, id: number, input: IEndNodeData) {
    super(module, { id, name: 'End Node', text: input.text });

    this.force = input.force ? input.force : false;
  }

  build(): IEndNode {
    const { id, text, force } = this;

    return {
      id,
      type: ENodeType.End,
      typeData: {
        text,
        force,
      },
    };
  }
}
