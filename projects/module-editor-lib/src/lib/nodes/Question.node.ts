import { Node } from './Node';
import { ENodeType, IAnswer, IQuestionNode, IQuestionNodeData } from '@suitless/module-domain';
import { Module } from '../Module';

export class QuestionNode extends Node {
  public multi: boolean;
  public useUniqueRoutes: boolean;
  public image: string;
  public examples: string[];
  public explanations: string[];

  constructor(module: Module, id: number, input: IQuestionNodeData) {
    super(module, { id, name: 'Question Node', text: input.text });

    this.multi = input.multi ? input.multi : false;
    this.useUniqueRoutes = input.useUniqueRoutes ? input.useUniqueRoutes : false;
    this.examples = input.examples ? input.examples : [];
    this.explanations = input.explanations ? input.explanations : [];
    this.image = input.image ? input.image : '';
  }

  build(): IQuestionNode {
    const { id, text, multi, useUniqueRoutes, examples, explanations, image } = this;

    const answers = this.flows.map((node) => node.build() as IAnswer);

    return {
      id,
      type: ENodeType.Question,
      typeData: {
        text,
        multi,
        image,
        useUniqueRoutes,
        examples,
        explanations,
        answers,
      },
    };
  }
}
