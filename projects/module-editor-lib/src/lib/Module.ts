import { AnswerNode, ImplicationNode, Node, NotificationNode, QuestionNode, StartNode } from './nodes';
import {
  ENodeType,
  IEndNode,
  IImplicationNode,
  IModule,
  INode,
  INotificationNode,
  IQuestionNode,
  IStartNode,
} from '@suitless/module-domain';
import { NodeInput } from './interfaces';
import { Cache } from './utils';
import { EndNode } from './nodes';

export class Module {
  public readonly nodes: Cache<number, Node>;

  constructor(
    public id: string,
    public name: string,
    public imageUri: string,
    public owner: string,
    public description: string,
    public disclaimer: string,
    public pdfDisclaimer: string,
    public version: number,
    public exposed: boolean = false,
    public tags: string[] = []
  ) {
    this.nodes = new Cache<number, Node>();
  }

  /**
   * Import a module
   * @param data The data to create the module from
   */
  public static Import(data: IModule): Promise<Module> {
    const modulePromise = new Promise<Module>((resolve) => {
      // Create the module
      const module = new Module(
        data.id,
        data.name,
        data.imageUri,
        data.owner,
        data.description,
        data.disclaimer,
        data.pdfDisclaimer,
        data.version,
        data.exposed,
        data.tags
      );

      const flowsToParse: Array<{ origin: number; target: number }> = [];

      for (const node of data.nodes) {
        let createdNode: Node;

        switch (node.type) {
          case ENodeType.Start:
            const startNode = node as IStartNode;
            createdNode = new StartNode(module, startNode.id, startNode.typeData);
            startNode.typeData.flows.map((flow) => flowsToParse.push({ origin: startNode.id, target: flow }));
            break;
          case ENodeType.Question:
            const questionNode = node as IQuestionNode;
            const createdQuestionNode = new QuestionNode(module, questionNode.id, questionNode.typeData);
            for (const answer of questionNode.typeData.answers) {
              const createdAnswerNode = new AnswerNode(module, answer.id, { text: answer.text, image: answer.image });
              createdQuestionNode.addNode(createdAnswerNode);
              answer.flows.map((flow) => flowsToParse.push({ origin: answer.id, target: flow }));
            }
            createdNode = createdQuestionNode;
            break;
          case ENodeType.Implication:
            const implicationNode = node as IImplicationNode;
            createdNode = new ImplicationNode(module, implicationNode.id, implicationNode.typeData);
            implicationNode.typeData.flows.map((flow) =>
              flowsToParse.push({ origin: implicationNode.id, target: flow })
            );
            break;
          case ENodeType.Notification:
            const notificationNode = node as INotificationNode;
            createdNode = new NotificationNode(module, notificationNode.id, notificationNode.typeData);
            notificationNode.typeData.flows.map((flow) =>
              flowsToParse.push({ origin: notificationNode.id, target: flow })
            );
            break;
          case ENodeType.End:
            const endNode = node as IEndNode;
            createdNode = new EndNode(module, endNode.id, endNode.typeData);
            break;
        }

        module.nodes.set(node.id, createdNode);
      }

      for (const { origin, target } of flowsToParse) {
        const originNode = module.nodes.get(origin);
        const targetNode = module.nodes.get(target);

        if (originNode && targetNode) {
          originNode.flows.set(targetNode.id, targetNode);
          targetNode.parents.set(originNode.id, originNode);
        }
      }

      resolve(module);
    });
    return modulePromise;
  }

  public static Export(module: Module): IModule {
    const nodes = module.nodes
      .map((node) => {
        if (!(node instanceof AnswerNode)) {
          return node.build() as INode;
        }
      })
      .filter((node) => node !== undefined);

    return {
      id: module.id,
      name: module.name,
      imageUri: module.imageUri,
      owner: module.owner,
      description: module.description,
      disclaimer: module.disclaimer,
      pdfDisclaimer: module.pdfDisclaimer,
      version: module.version,
      exposed: module.exposed,
      tags: module.tags,
      nodes,
    };
  }

  /**
   * Adds a new node to the module
   * @param type The node type to add
   * @param input The data to add to the node
   */
  public addNode<N>(type: new (module: Module, id: number, input: NodeInput) => N, input: NodeInput): N {
    const createdNode = Node.Create(this, type, input);
    const node = (createdNode as any) as Node;
    this.nodes.set(node.id, node);
    return createdNode;
  }
}
