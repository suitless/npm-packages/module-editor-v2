export class Cache<K, V> extends Map<K, V> {
  public get first(): V {
    return this.values().next().value;
  }

  public toKeyArray(): K[] {
    return [...this.keys()];
  }

  public toValueArray(): V[] {
    return [...this.values()];
  }

  public map<T>(func: (value: V, key: K) => T): T[] {
    const mapIter: IterableIterator<[K, V]> = this.entries();
    return Array.from(
      { length: this.size },
      (): T => {
        const [key, val]: [K, V] = mapIter.next().value;
        return func(val, key);
      }
    );
  }
}
