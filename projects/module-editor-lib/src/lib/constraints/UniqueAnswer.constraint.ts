import { IConstraint, INode, IError, ENodeType, EErrorType, IQuestionNode } from '@suitless/module-domain';

/**
 * Checks each question for duplicate answers.
 */
export class UniqueAnswerConstraint implements IConstraint {
  public name = 'Duplicate Answer';
  public description = "Each answer should be unique, otherwise the user can't make an educated decision";
  public global = false;

  constructor() {}

  public run(input: INode): IError[] {
    let result: IError[] = [];
    if (input.type == ENodeType.Question) {
      let node = input as IQuestionNode;
      let answers: string[] = [];
      for (let ans of node.typeData.answers) {
        if (answers.includes(ans.text.toUpperCase())) {
          result.push(this.generateError(node, ans.text));
        } else {
          answers.push(ans.text.toUpperCase());
        }
      }
    }
    return result;
  }

  private generateError(node: INode, text: string): IError {
    return {
      name: this.name,
      description: this.description,
      global: this.global,
      severeness: EErrorType.Critical,
      node: node.id,
      message: `Question with duplicate answer, at: "${text}".`
    };
  }
}
