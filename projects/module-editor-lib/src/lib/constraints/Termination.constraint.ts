import {
  IConstraint,
  INode,
  IError,
  ENodeType,
  IStartNode,
  EErrorType,
  IQuestionNode,
  INotificationNode
} from '@suitless/module-domain';

/**
 * Checks each field of a node for period termination.
 */
export class TerminationConstraint implements IConstraint {
  public name = 'Text termination';
  public description =
    'Each text should be terminated properly, for example using a period. (eg. "We are suitless" -> "We are suitless.")';
  public global = false;

  constructor() {}

  public run(input: INode): IError[] {
    let result: IError[] = [];
    if (input.type == ENodeType.Start) {
      let node = input as IStartNode;
      if (this.test(node.typeData.text)) {
        result.push(this.generateError(node, 'title', node.typeData.text));
      }
      if (this.test(node.typeData.description)) {
        result.push(this.generateError(node, 'description', node.typeData.description));
      }
    } else if (input.type == ENodeType.Question) {
      let node = input as IQuestionNode;
      if (this.test(node.typeData.text)) {
        result.push(this.generateError(node, 'question', node.typeData.text));
      }
      for (let example of node.typeData.examples) {
        if (this.test(example)) {
          result.push(this.generateError(node, 'example', example));
        }
      }
      for (let explanation of node.typeData.explanations) {
        if (this.test(explanation)) {
          result.push(this.generateError(node, 'explanation', explanation));
        }
      }
      for (let answer of node.typeData.answers) {
        if (this.test(answer.text)) {
          result.push(this.generateError(node, 'answer', answer.text));
        }
      }
    } else if (
      input.type == ENodeType.Notification ||
      input.type == ENodeType.Implication ||
      input.type == ENodeType.End
    ) {
      let node = input as INotificationNode;
      if (this.test(node.typeData.text)) {
        result.push(this.generateError(node, 'text', node.typeData.text));
      }
    }
    return result;
  }

  /**
   * returns true if the last character is not a '.'.
   * @param input text
   */
  private test(input: string): boolean {
    return !/.*[\.\!\?\"\)\]\}\>\'\`]/.test(input);
  }

  private generateError(node: INode, field: string, text: string): IError {
    return {
      name: this.name,
      description: this.description,
      global: this.global,
      severeness: EErrorType.Suggestion,
      node: node.id,
      message: `The text of the ${field} does not end with a punctuation- or reading mark (eg. ".", "?", ")", ">"), at: "${text}".`
    };
  }
}
