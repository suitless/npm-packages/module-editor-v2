import { IConstraint, INode, IError, ENodeType, EErrorType, IQuestionNode } from '@suitless/module-domain';

/**
 * Checks each question question mark.
 */
export class QuestionMarkConstraint implements IConstraint {
  public name = 'Question mark';
  public description = 'Each question should end with a question mark. (eg. "How are you" -> "How are you?")';
  public global = false;

  constructor() {}

  public run(input: INode): IError[] {
    let result: IError[] = [];
    if (input.type == ENodeType.Question) {
      let node = input as IQuestionNode;
      if (this.test(node.typeData.text)) {
        result.push(this.generateError(node, node.typeData.text));
      }
    }
    return result;
  }

  /**
   * returns true if the last character is not a '?'.
   * @param input text
   */
  private test(input: string): boolean {
    return !(input.charAt(input.length - 1) == '?') && input.length > 0;
  }

  private generateError(node: INode, text: string): IError {
    return {
      name: this.name,
      description: this.description,
      global: this.global,
      severeness: EErrorType.Suggestion,
      node: node.id,
      message: `Question does not end with a question mark ("?"), at: "${text}".`
    };
  }
}
