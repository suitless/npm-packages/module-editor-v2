import { IQuestionNode, ENodeType, IImplicationNode, IError } from '@suitless/module-domain';
import { EmptyFieldConstraint } from './EmptyField.constraint';

describe('EmptyFieldConstraint', () => {
  it('should detect empty fields', () => {
    let constraint = new EmptyFieldConstraint();
    let results: IError[];
    results = constraint.run({
      id: 0,
      type: ENodeType.Implication,
      typeData: {
        flows: [],
        level: '',
        text: ''
      }
    } as IImplicationNode);
    expect(results.length).toBe(2);

    results = constraint.run({
      id: 0,
      type: ENodeType.Question,
      typeData: {
        answers: [
          {
            id: 0,
            flows: [],
            text: ''
          }
        ],
        examples: ['I am capitalized.'],
        explanations: [''],
        multi: false,
        useUniqueRoutes: false,
        text: ''
      }
    } as IQuestionNode);
    expect(results.length).toBe(3);
  });

  it('should detect filled fields', () => {
    let constraint = new EmptyFieldConstraint();
    let results: IError[];
    results = constraint.run({
      id: 0,
      type: ENodeType.Implication,
      typeData: {
        flows: [],
        level: 'lmao',
        text: 'i am not capitalized.'
      }
    } as IImplicationNode);
    expect(results.length).toBe(0);

    results = constraint.run({
      id: 0,
      type: ENodeType.Question,
      typeData: {
        answers: [
          {
            id: 0,
            flows: [],
            text: 'i am capitalized.'
          }
        ],
        examples: ['i am capitalized.'],
        explanations: ['i am capitalized.'],
        multi: false,
        useUniqueRoutes: false,
        text: 'i am capitalized.'
      }
    } as IQuestionNode);
    expect(results.length).toBe(0);
  });
});
