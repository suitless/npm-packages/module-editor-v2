import { IConstraint, INode, IError, ENodeType, EErrorType, IQuestionNode, IStartNode } from '@suitless/module-domain';

/**
 * Checks each node for empty flows.
 */
export class EmptyFlowsConstraint implements IConstraint {
  public name = 'Empty flows';
  public description = 'Each node should flow into an end node downstream, otherwise the user can reach a dead-end.';
  public global = false;

  constructor() {}

  public run(input: INode): IError[] {
    let result: IError[] = [];
    if (input.type != ENodeType.End) {
      if (input.type == ENodeType.Question) {
        const node = input as IQuestionNode;
        for (const ans of node.typeData.answers) {
          if (ans.flows.length == 0) {
            result.push(this.generateError(node));
          }
        }
      } else if (input.type != undefined) {
        const node = input as IStartNode;
        if (node.typeData.flows.length == 0) {
          result.push(this.generateError(node));
        }
      }
    }
    return result;
  }

  private generateError(node: INode): IError {
    return {
      name: this.name,
      description: this.description,
      global: this.global,
      severeness: EErrorType.Critical,
      node: node.id,
      message: `Node with no attached nodes downstream.`
    };
  }
}
