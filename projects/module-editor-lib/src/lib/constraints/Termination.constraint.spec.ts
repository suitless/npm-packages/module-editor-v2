import { IQuestionNode, ENodeType, IError, IStartNode } from '@suitless/module-domain';
import { TerminationConstraint } from './Termination.constraint';

describe('TerminationConstraint', () => {
  it('should not detect period terminated fields', () => {
    let constraint = new TerminationConstraint();
    let results: IError[];
    results = constraint.run({
      id: 0,
      type: ENodeType.Start,
      typeData: {
        flows: [],
        description: 'I end with a period.',
        text: 'I end with a exlamation mark!'
      }
    } as IStartNode);
    expect(results.length).toBe(0);
  });

  it('should detect unperiod terminated fields', () => {
    let constraint = new TerminationConstraint();
    let results: IError[];
    results = constraint.run({
      id: 0,
      type: ENodeType.Question,
      typeData: {
        answers: [
          {
            id: 0,
            flows: [],
            text: 'i am capitalized'
          }
        ],
        examples: ['i am capitalized'],
        explanations: ['i am capitalized'],
        multi: false,
        useUniqueRoutes: false,
        text: 'I am ignored?'
      }
    } as IQuestionNode);
    expect(results.length).toBe(3);
  });
});
