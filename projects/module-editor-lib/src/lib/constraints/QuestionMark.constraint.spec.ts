import { IQuestionNode, ENodeType, IError, IStartNode } from '@suitless/module-domain';
import { QuestionMarkConstraint } from './QuestionMark.constraint';

describe('QuestionMarkConstraint', () => {
  it('should not detect questionMarked fields', () => {
    let constraint = new QuestionMarkConstraint();
    let results: IError[];
    results = constraint.run({
      id: 0,
      type: ENodeType.Question,
      typeData: {
        answers: [
          {
            id: 0,
            flows: [],
            text: 'I am capitalized.'
          }
        ],
        examples: ['I am capitalized.'],
        explanations: ['I am capitalized.'],
        multi: false,
        useUniqueRoutes: false,
        text: 'I am questionmarked?'
      }
    } as IQuestionNode);
    expect(results.length).toBe(0);
  });

  it('should detect unquestionmarked fields', () => {
    let constraint = new QuestionMarkConstraint();
    let results: IError[];
    results = constraint.run({
      id: 0,
      type: ENodeType.Question,
      typeData: {
        answers: [
          {
            id: 0,
            flows: [],
            text: 'i am capitalized.'
          }
        ],
        examples: ['i am capitalized.'],
        explanations: ['i am capitalized.'],
        multi: false,
        useUniqueRoutes: false,
        text: 'i am not questionmarked.'
      }
    } as IQuestionNode);
    expect(results.length).toBe(1);
  });

  it('should ignore other nodes', () => {
    let constraint = new QuestionMarkConstraint();
    let results: IError[];
    results = constraint.run({
      id: 0,
      type: ENodeType.Start,
      typeData: {
        description: 'yep.',
        flows: [],
        text: 'lamo'
      }
    } as IStartNode);
    expect(results.length).toBe(0);
  });
});
