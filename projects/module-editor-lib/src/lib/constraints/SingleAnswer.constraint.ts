import { IConstraint, INode, IError, ENodeType, EErrorType, IQuestionNode } from '@suitless/module-domain';

/**
 * Checks each question for single answers.
 */
export class SingleAnswerConstraint implements IConstraint {
  public name = 'Single Answer';
  public description = "Each question should have multiple answers, otherwise the user can't make a decision";
  public global = false;

  constructor() {}

  public run(input: INode): IError[] {
    let result: IError[] = [];
    if (input.type == ENodeType.Question) {
      let node = input as IQuestionNode;
      if (node.typeData.answers.length < 2) {
        result.push(this.generateError(node, node.typeData.answers.length));
      }
    }
    return result;
  }

  private generateError(node: INode, answers: number): IError {
    return {
      name: this.name,
      description: this.description,
      global: this.global,
      severeness: EErrorType.Warning,
      node: node.id,
      message: `The question ${answers == 0 ? 'contains no answers' : `only contains ${answers} answer.`}`
    };
  }
}
