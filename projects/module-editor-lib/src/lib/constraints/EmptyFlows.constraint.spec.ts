import { IQuestionNode, ENodeType, IImplicationNode, IError } from '@suitless/module-domain';
import { EmptyFlowsConstraint } from './EmptyFlows.constraint';

describe('EmptyFlowsConstraint', () => {
  it('should detect empty flows', () => {
    let constraint = new EmptyFlowsConstraint();
    let results: IError[];
    results = constraint.run({
      id: 0,
      type: ENodeType.Question,
      typeData: {
        answers: [
          {
            id: 0,
            flows: [],
            text: ''
          },
          {
            id: 1,
            flows: [2],
            text: ''
          }
        ],
        examples: ['I am capitalized.'],
        explanations: [''],
        multi: false,
        useUniqueRoutes: false,
        text: ''
      }
    } as IQuestionNode);
    expect(results.length).toBe(1);
  });

  it('should not detect filled flows', () => {
    let constraint = new EmptyFlowsConstraint();
    let results: IError[];
    results = constraint.run({
      id: 0,
      type: ENodeType.Question,
      typeData: {
        answers: [
          {
            id: 0,
            flows: [2],
            text: 'i am capitalized.'
          }
        ],
        examples: ['i am capitalized.'],
        explanations: ['i am capitalized.'],
        multi: false,
        useUniqueRoutes: false,
        text: 'i am capitalized.'
      }
    } as IQuestionNode);
    expect(results.length).toBe(0);
  });
});
