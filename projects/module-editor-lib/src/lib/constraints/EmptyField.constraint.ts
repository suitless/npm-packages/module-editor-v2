import {
  IConstraint,
  INode,
  IError,
  ENodeType,
  IStartNode,
  EErrorType,
  IQuestionNode,
  INotificationNode,
  IImplicationNode
} from '@suitless/module-domain';

/**
 * Checks each field of a node for emptyness.
 */
export class EmptyFieldConstraint implements IConstraint {
  public name = 'Empty field';
  public description = 'Each field should be filled with content. (eg. "" -> "How are you?")';
  public global = false;

  constructor() {}

  public run(input: INode): IError[] {
    let result: IError[] = [];
    if (input.type == ENodeType.Start) {
      let node = input as IStartNode;
      if (this.test(node.typeData.text)) {
        result.push(this.generateError(node, 'title'));
      }
      if (this.test(node.typeData.description)) {
        result.push(this.generateError(node, 'description'));
      }
    } else if (input.type == ENodeType.Question) {
      let node = input as IQuestionNode;
      if (this.test(node.typeData.text)) {
        result.push(this.generateError(node, 'question'));
      }
      for (let example of node.typeData.examples) {
        if (this.test(example)) {
          result.push(this.generateError(node, 'example'));
        }
      }
      for (let explanation of node.typeData.explanations) {
        if (this.test(explanation)) {
          result.push(this.generateError(node, 'explanation'));
        }
      }
      for (let answer of node.typeData.answers) {
        if (this.test(answer.text)) {
          result.push(this.generateError(node, 'answer'));
        }
      }
    } else if (input.type == ENodeType.Implication) {
      let node = input as IImplicationNode;
      if (this.test(node.typeData.text)) {
        result.push(this.generateError(node, 'text'));
      }
      if (this.test(node.typeData.level)) {
        result.push(this.generateError(node, 'level'));
      }
    } else if (input.type == ENodeType.Notification || input.type == ENodeType.End) {
      let node = input as INotificationNode;
      if (this.test(node.typeData.text)) {
        result.push(this.generateError(node, 'text'));
      }
    }
    return result;
  }

  /**
   * returns true if empty.
   * @param input text
   */
  private test(input: string): boolean {
    return input.length === 0;
  }

  private generateError(node: INode, field: string): IError {
    return {
      name: this.name,
      description: this.description,
      global: this.global,
      severeness: EErrorType.Critical,
      node: node.id,
      message: `There is no content for the ${field}.`
    };
  }
}
