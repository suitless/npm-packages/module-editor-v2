import {
  IConstraint,
  INode,
  IError,
  ENodeType,
  IStartNode,
  EErrorType,
  IQuestionNode,
  INotificationNode
} from '@suitless/module-domain';

/**
 * Checks each field of a node for capitalization.
 */
export class CapitalizationConstraint implements IConstraint {
  public name = 'Capitalization';
  public description = 'Each field should start with a capital letter. (eg. "how are you?" -> "How are you?")';
  public global = false;

  constructor() {}

  public run(input: INode): IError[] {
    let result: IError[] = [];
    if (input.type == ENodeType.Start) {
      let node = input as IStartNode;
      if (this.test(node.typeData.text)) {
        result.push(this.generateError(node, 'title', node.typeData.text));
      }
      if (this.test(node.typeData.description)) {
        result.push(this.generateError(node, 'description', node.typeData.description));
      }
    } else if (input.type == ENodeType.Question) {
      let node = input as IQuestionNode;
      if (this.test(node.typeData.text)) {
        result.push(this.generateError(node, 'question', node.typeData.text));
      }
      for (let example of node.typeData.examples) {
        if (this.test(example)) {
          result.push(this.generateError(node, 'example', example));
        }
      }
      for (let explanation of node.typeData.explanations) {
        if (this.test(explanation)) {
          result.push(this.generateError(node, 'explanation', explanation));
        }
      }
      for (let answer of node.typeData.answers) {
        if (this.test(answer.text)) {
          result.push(this.generateError(node, 'answer', answer.text));
        }
      }
    } else if (
      input.type == ENodeType.Notification ||
      input.type == ENodeType.Implication ||
      input.type == ENodeType.End
    ) {
      let node = input as INotificationNode;
      if (this.test(node.typeData.text)) {
        result.push(this.generateError(node, 'text', node.typeData.text));
      }
    }
    return result;
  }

  /**
   * returns true if not capitalized.
   * @param input text
   */
  private test(input: string): boolean {
    return !/^[A-Z0-9]/.test(input.charAt(0)) && input.length > 0;
  }

  private generateError(node: INode, field: string, text: string): IError {
    return {
      name: this.name,
      description: this.description,
      global: this.global,
      severeness: EErrorType.Suggestion,
      node: node.id,
      message: `First letter of the ${field} is not a capital letter, at: "${text}".`
    };
  }
}
