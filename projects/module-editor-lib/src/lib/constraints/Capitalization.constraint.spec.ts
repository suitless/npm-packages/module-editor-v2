import { CapitalizationConstraint } from './Capitalization.constraint';
import { IQuestionNode, ENodeType, IImplicationNode, IError } from '@suitless/module-domain';

describe('CapitalizationConstraint', () => {
  it('should not detect capitalized fields', () => {
    let constraint = new CapitalizationConstraint();
    let results: IError[];
    results = constraint.run({
      id: 0,
      type: ENodeType.Implication,
      typeData: {
        flows: [],
        level: 'lmao',
        text: 'I am capitalized.'
      }
    } as IImplicationNode);
    expect(results.length).toBe(0);

    results = constraint.run({
      id: 0,
      type: ENodeType.Question,
      typeData: {
        answers: [
          {
            id: 0,
            flows: [],
            text: 'I am capitalized.'
          }
        ],
        examples: ['I am capitalized.'],
        explanations: ['I am capitalized.'],
        multi: false,
        useUniqueRoutes: false,
        text: 'I am capitalized.'
      }
    } as IQuestionNode);
    expect(results.length).toBe(0);
  });

  it('should detect uncapitalized fields', () => {
    let constraint = new CapitalizationConstraint();
    let results: IError[];
    results = constraint.run({
      id: 0,
      type: ENodeType.Implication,
      typeData: {
        flows: [],
        level: 'lmao',
        text: 'i am not capitalized.'
      }
    } as IImplicationNode);
    expect(results.length).toBe(1);

    results = constraint.run({
      id: 0,
      type: ENodeType.Question,
      typeData: {
        answers: [
          {
            id: 0,
            flows: [],
            text: 'i am capitalized.'
          }
        ],
        examples: ['i am capitalized.'],
        explanations: ['i am capitalized.'],
        multi: false,
        useUniqueRoutes: false,
        text: 'i am capitalized.'
      }
    } as IQuestionNode);
    expect(results.length).toBe(4);
  });

  it('should not detect numbers ', () => {
    let constraint = new CapitalizationConstraint();
    let results: IError[];
    results = constraint.run({
      id: 0,
      type: ENodeType.Implication,
      typeData: {
        flows: [],
        level: '2 lmao',
        text: '1 i am not capitalized.'
      }
    } as IImplicationNode);
    expect(results.length).toBe(0);

    results = constraint.run({
      id: 0,
      type: ENodeType.Question,
      typeData: {
        answers: [
          {
            id: 0,
            flows: [],
            text: 'i am capitalized.'
          }
        ],
        examples: ['5 i am capitalized.'],
        explanations: ['7 i am capitalized.'],
        multi: false,
        useUniqueRoutes: false,
        text: 'i am capitalized.'
      }
    } as IQuestionNode);
    expect(results.length).toBe(2);
  });
});
