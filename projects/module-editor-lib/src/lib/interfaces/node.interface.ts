import {
  IEndNodeData,
  IImplicationNodeData,
  INotificationNodeData,
  IQuestionNodeData,
  IStartNodeData,
} from '@suitless/module-domain';

export interface IAnswerNodeData {
  image: string;
  text: string;
}

export type NodeInput =
  | IStartNodeData
  | IQuestionNodeData
  | IAnswerNodeData
  | INotificationNodeData
  | IImplicationNodeData
  | IEndNodeData;

export interface INodeOptions {
  id: number;
  name: string;
  text: string;
}
